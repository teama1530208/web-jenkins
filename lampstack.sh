#!/bin/bash  

# Update system  
sudo apt-get update -y  
   
## Install APache  
sudo apt-get install apache2 apache2-doc apache2-mpm-prefork apache2-utils libexpat1 ssl-cert -y  
   
## Install PHP  
apt-get install php libapache2-mod-php php-mysql -y  
   
# Install MySQL database server  
apt-get install mysql-server -y  
   
# Restart Apache  
sudo service apache2 restart  
